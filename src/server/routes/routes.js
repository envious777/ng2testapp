var express = require('express');
var actions = require('./../methods/actions');
var corporate = require('../model/corporate');
var config = require('../config/database');

var router = express.Router();

router.get('/getallcorp', actions.getAllData);
router.post('/addcorp', actions.addData);
router.get('/corp/:id', actions.getData);
router.put('/corp/:id', actions.updateData);
router.delete('/corp/:id', function(req, res){
     corporate.findOneAndRemove({sicode: req.params.id}, function(err) {
        if(err) 
            return console.error(err);
        res.sendStatus(200);
    });        
});

module.exports = router;