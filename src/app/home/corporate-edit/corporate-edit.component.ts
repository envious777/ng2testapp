import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

// import { Corporate } from '../../corporate';
import { CorporateService } from '../corporateservice.service';

@Component({
  selector: 'app-corporate-edit',
  templateUrl: './corporate-edit.component.html',
  styleUrls: ['./corporate-edit.component.css'],
  providers: [CorporateService]
})
export class CorporateEditComponent implements OnInit, OnDestroy {
  result: any;
  private sub: Subscription;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private corpser: CorporateService) {}

  ngOnInit(): void {
        this.sub = this.route.params.subscribe(
            params => {
                let id = +params['id'];
                this.getData(id);
        });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getData(sicode) {
    this.corpser.getData(sicode).subscribe(
      data => {
        console.log(data);
        this.result = data;
      },
      error => console.log(error)
    );
  }
}
