import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Corporate } from '../Corporate';

@Injectable()

export class CorporateService {
  private headers = new Headers({'Content-Type': 'application/json', 'charset': 'UTF-8'});
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  saveData(form) {
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    // return new Promise(resolve => {
    //   this.http.post('http://localhost:8080/addcorp', JSON.stringify(form), {headers: headers}).subscribe(data => {
    //     if(data.json().success){
    //         resolve(true);
    //     }
    //     else{
    //       resolve(false);
    //       console.log("error message", data.json().message);
    //       console.log("request object", data.json().request);
    //     }
    //   });
    // });

    return this.http.post('http://localhost:8080/addcorp', JSON.stringify(form), this.options);

    // let forms = this.getAllData();
    // if (forms === '') {
    //   forms = [];
    //   forms.push(form);
    //   localStorage.setItem('forms', JSON.stringify(forms));
    // } else {
    //   forms.push(form);
    //   localStorage.setItem('forms', JSON.stringify(forms));
    // }
  }

  getAllData() {
    // if (localStorage.getItem('forms') === null || localStorage.getItem('forms') === undefined || localStorage.getItem('forms') === '') {
    //   localStorage.setItem('forms', '');
    //   let forms = localStorage.getItem('forms');
    //   return forms;
    // } else {
    //   let forms = JSON.parse(localStorage.getItem('forms'));
    //   return forms;
    // }

    return this.http.get('http://localhost:8080/getallcorp').map(res => res.json());    
  }

  getData(sicode) {
    return this.http.get(`http://localhost:8080/corp/${sicode}`, this.options).map(res => res.json());
  }

  editData(form) {
    // let f = this.getAllData();
    // let i = f.findIndex(obj => obj.sicode === oldform.sicode);
    // f.splice(i, 1);
    // f.push(newform);
    // localStorage.setItem('forms', JSON.stringify(f));
    return this.http.put(`http://localhost:8080/corp/${form.sicode}`, JSON.stringify(form), this.options);
  }

  deleteData(sicode){
    // console.log("Corp to be Deleted:", delform);
    // let f = this.getAllData();
    // let i = f.findIndex(obj => obj.sicode === delform.sicode);
    // f.splice(i, 1);
    // localStorage.setItem('forms', JSON.stringify(f));
    return this.http.delete(`http://localhost:8080/corp/${sicode}`, this.options);
  }

}
