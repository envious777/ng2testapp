import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ng2-bootstrap/components/alert';
import { HttpModule } from '@angular/http';
import { SharedModule } from './../shared/shared.module';

import { homeRouting } from './home.routing';
import { CorporateCreateComponent } from  './corporate-create/corporate-create.component';
import { CorporateEditComponent } from  './corporate-edit/corporate-edit.component';
import { CorporateViewComponent } from  './corporate-view/corporate-view.component';

import { CorporateService } from './corporateservice.service';


@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AlertModule,
    HttpModule,
    homeRouting
  ],
  declarations: [
    CorporateCreateComponent,
    CorporateEditComponent,
    CorporateViewComponent
  ],
  providers: [CorporateService]
})
export class HomeModule {}
