import { Component, OnInit, Input, trigger, state, style, transition, animate, HostBinding } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CorporateService } from '../corporateservice.service';
import { Corporate } from '../../corporate';


@Component({
  selector: 'app-corporate-create',
  templateUrl: './corporate-create.component.html',
  styleUrls: ['./corporate-create.component.css'],
  providers: [FormBuilder, CorporateService],
  animations: [
    trigger('flyInOut', [
      state('in', style({opacity: 1, transform: 'translateY(0)'})),
      transition('void => *', [
        style({transform: 'translateY(25%)',opacity: 0}),
        animate('1s cubic-bezier(0,1.02,0.56,1)')
      ]),
      transition('* => void', [
        animate('0s ease', style({transform: 'translateY(100%)',opacity: 0}))
      ])
    ])
  ],
})

export class CorporateCreateComponent implements OnInit {
  @HostBinding('@flyInOut') get fadeInflyOut(){
    return true;
  }
  @HostBinding('style.display') get display(){
    return 'block';
  }
  @HostBinding('style.position') get position(){
    return 'relative';
  }

  @Input() resfromedit: Corporate;
  location: string = '';
  forms: any = [];
  dataModel: any = {};
  f: Corporate;

  constructor(private router: Router, private corpser: CorporateService) {
    this.location = router.url;
  }

  public alerts:Array<Object> = [];

  ngOnInit() {
  }

  submitForm(form: Corporate) {
    // this.f = new Corporate(form.address, form.corpname, form.corptype, form.excode, form.exname, form.sicode,
    // form.sindeb, form.cardno, form.authmode);
    if (this.location === '/corporate-create') {
      this.corpser.saveData(form).subscribe(
        result => {
          console.log('Corporate Created');
          this.alerts.push({msg: 'Corporate Succesfully Created', type: 'success', closable: true});
          this.router.navigate(['/Home']);
        },
        error => console.log(error)
      );
    } else {
      console.log(form);
      this.corpser.editData(form).subscribe(
        result => {
          this.alerts.push({msg: 'Corporate Details Updated', type: 'success', closable: true});
        },
        error => console.log(error)
      );
    }
  }

  deleteForm(sicode){
    // this.corpser.deleteData(this.resfromedit.sicode);
    console.log(sicode);
    this.corpser.deleteData(sicode).subscribe(
        result => {
          this.alerts.push({msg: 'Corporate Deleted', type: 'danger', closable: true});
          // this.router.navigate(['/Home']);
        },
        error => console.log(error)
    );
    
  }

  closeAlert(i: number) {
    this.alerts.splice(i, 1);
  }

  // form: FormGroup;
  // address:any;
  // value: any;

  // constructor(fb: FormBuilder) {
  //   this.form = fb.group({
  //     'corpname': [null, Validators.required],
  //     'excode': [null, Validators.required],
  //     'exname': [null, Validators.required],
  //     'corptype': [null, Validators.required],
  //     'cardno': '',
  //     'sicode': '',
  //     'sindeb': false,
  //     'authmode': [null, Validators.required]
  //   });
  // }

  // address: any;
  // value: any; 
  // constructor() {
  //   this.value = {
  //     address: {}
  //   }
  // }
  // submitForm(value){
  //   console.log("Form Data: ");
  //   console.log(value);
  //   console.log('Form Address: ', this.value);
  // }

  // value: any = {};
  // formUpdated(event){
  //   Object.assign(this.value, {adress: event});
  // }
  // submit(form) {
  //   // this.value = form;
  //   Object.assign(this.value, form);
  //   console.log(this.value);
  // }

}
