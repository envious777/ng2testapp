import { Component, OnInit, trigger, state, style, transition, animate, HostBinding } from '@angular/core';
import { CorporateService } from './corporateservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CorporateService],
  animations: [
    trigger('fadeInflyOut', [
      state('in', style({opacity: 1, transform: 'scale(1)'})),
      transition('void => *', [
        style({opacity: 0, transform: 'scale(1.1)'}),
        animate('0.5s ease')
      ]),
      // transition('* => void', [
      //   animate('0.05s ease', style({opacity: 0}))
      // ])
    ])
  ]
})
export class HomeComponent implements OnInit {
  @HostBinding('@fadeInflyOut') get fadeInflyOut(){
    return true;
  }
  @HostBinding('style.display') get display(){
    return 'block';
  }
  @HostBinding('style.position') get position(){
    return 'relative';
  }

  corps: any;

  constructor(private corpser: CorporateService) { }

  ngOnInit() {
    this.corpser.getAllData().subscribe(
      data => this.corps = data,
      error => console.log(error)
    );
  }

}
