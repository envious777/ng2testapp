import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorporateCreateComponent } from  './corporate-create/corporate-create.component';
import { CorporateEditComponent } from  './corporate-edit/corporate-edit.component';
import { CorporateViewComponent } from  './corporate-view/corporate-view.component';

const homeRoutes: Routes = [
  { path: 'corporate-create', component: CorporateCreateComponent },
  { path: 'corporate-view/:id', component: CorporateViewComponent },
  { path: 'corporate-edit/:id', component: CorporateEditComponent }
];

export const homeRouting: ModuleWithProviders =
               RouterModule.forRoot(homeRoutes);
