/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CorporateserviceService } from './corporateservice.service';

describe('Service: Corporateservice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CorporateserviceService]
    });
  });

  it('should ...', inject([CorporateserviceService], (service: CorporateserviceService) => {
    expect(service).toBeTruthy();
  }));
});
