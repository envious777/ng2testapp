import { Ng2testappPage } from './app.po';

describe('ng2testapp App', function() {
  let page: Ng2testappPage;

  beforeEach(() => {
    page = new Ng2testappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
