var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var corporateSchema = new Schema({
    address: {
        addr1: {
             type: String,
             required: true
        },
        addr2: {
             type: String
        }
    },
   corpname: {
      type: String,
      required: true
    },
    corptype: {
      type: String,
    },
    excode: {
      type: String,
    },
    exname: {
      type: String,
    },
    sicode: {
      type: Number,
      required: true
    },
    sindeb: {
      type: String,
    },
    cardno: {
      type: String,
    },
    authmode: {
      type: String,
    }
});

module.exports = mongoose.model('corporate', corporateSchema);



// db.corporate.insert({
//     address: {
//         addr1: 'abcd',
//         addr2: 'efgh'
//     },
//     corpname: 'test1',
//     corptype: 'Type 2',
//     excode: '878d',
//     exname: 'sadjns',
//     sicode: '1234',
//     sindeb: 'True',
//     cardno: '238934',
//     authmode: 'Mode 1',
// })