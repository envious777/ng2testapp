import { NgModule }  from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddressComponent } from './address/address.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports : [
    CommonModule,
    AddressComponent
  ],
  declarations: [ AddressComponent ],
})
export class SharedModule { }
