import { Component, OnInit, OnDestroy, trigger, state, transition, animate, style, HostBinding} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { CorporateService } from '../corporateservice.service';

@Component({
  selector: 'app-corporate-view',
  templateUrl: './corporate-view.component.html',
  styleUrls: ['./corporate-view.component.css'],
  providers: [CorporateService],
  animations: [
    trigger('flyInOut', [
      state('in', style({opacity: 1, transform: 'translateY(0)'})),
      transition('void => *', [
        style({transform: 'translateY(25%)',opacity: 0}),
        animate('1s cubic-bezier(0,1.02,0.56,1)')
      ]),
      transition('* => void', [
        animate('0s ease', style({transform: 'translateY(100%)',opacity: 0}))
      ])
    ])
  ]
})
export class CorporateViewComponent implements OnInit, OnDestroy{
  @HostBinding('@flyInOut') get fadeInflyOut(){
    return true;
  }
  @HostBinding('style.display') get display(){
    return 'block';
  }
  @HostBinding('style.position') get position(){
    return 'relative';
  }
  result: any;
  private sub: Subscription;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private corpser: CorporateService) {}

  ngOnInit(){
        this.sub = this.route.params.subscribe(
            params => {
                let id = +params['id'];
                this.getData(id);
        });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getData(sicode) {
    this.corpser.getData(sicode).subscribe(
      data => {
        console.log(data);
        this.result = data;
      },
      error => console.log(error)
    );
  }

}
