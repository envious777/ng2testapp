import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AddressComponent),
    multi: true
};

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css'],
   providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
  export class AddressComponent implements ControlValueAccessor {
    @Input() addr: any;
    // The internal data model
    private address: any = {};

    // Placeholders for the callbacks which are later providesd
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    // get accessor
    get value(): any {
        return this.address;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.address) {
            this.address = v;
            this.onChangeCallback(v);
        }
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.address) {
            this.address = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

  // writeValue(value: any) {
  //   if (value !== undefined) {
  //     this.address = value;
  //   }
  // }

  // registerOnChange() {}

  // registerOnTouched() {}


  // @Input() address: any = {};
  // constructor() {}

  // @Output() formUpdate = new EventEmitter();
  // addrForm: FormGroup;

  // constructor(fb: FormBuilder) {
  //   this.addrForm = fb.group({
  //     'street': [null, Validators.required],
  //     'zip': [null, Validators.required],
  //     'city': [null, Validators.required]
  //   });

  //   this.addrForm.valueChanges.subscribe((data) => {
  //     this.formUpdate.emit(data); ;
  //   });
  // }
}
