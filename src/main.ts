import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'; // compile templates on the fly
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { AppModule } from './app/'; // app folder's index.ts

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
