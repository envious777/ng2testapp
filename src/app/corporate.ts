export class Corporate {
    constructor(
        public address: {
            addr1: string,
            addr2: string
        },
        public corpname: string,
        public corptype: string,
        public excode: string,
        public exname: string,
        public sicode: number,
        public sindeb: string,
        public cardno: string,
        public authmode: string
    ){}
    
}
