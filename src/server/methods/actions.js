var corporate = require('../model/corporate');
var config = require('../config/database');

var functions = {
    getAllData: function(req, res) {
       corporate.find(function(err, corps) {
           if(err)
               res.send(err);
            else
                res.json(corps);
       });
    },

    addData: function(req, res){
        // var corp = new corporate();
        // var request = JSON.parse(req.body);
        //     corp.address = request.address;
        //     corp.corpname = request.corpname;
        //     corp.corptype = request.corptype;
        //     corp.excode = request.excode;
        //     corp.exname = request.exname;
        //     corp.sicode = request.sicode;
        //     corp.sindeb = request.sindeb;
        //     corp.cardno = request.cardno;
        //     corp.authmode = request.authmode;

        //     corp.save(function(err){
        //         if(err){
        //             res.send({message: err, success: false, request: request});
        //         } else {
        //             res.json({message: 'Corporate Created', success: true});
        //         }
        //     });
        var corp = new corporate(req.body);
        corp.save(function(err,corp){
            if(err)
                res.send({error: err, success: false});
            else
                res.status(200).json({success:true})
        });
    },

    getData: function(req, res){
        corporate.findOne({sicode: req.params.id}, function(err,corp){
            if(err)
                res.send(err);
            else
                res.json(corp);  
        });
    },

    updateData: function(req, res){
        corporate.findOneAndUpdate({sicode: req.params.id}, req.body, function(err){
            if(err) return console.error(err);
            res.sendStatus(200);
        });
    },

    // deleteData: function(req, res){
    //     corporate.findOneAndRemove({sicode: req.params.id}, function(err) {
    //         if(err) return console.error(err);
    //         res.sendStatus(200);
    //         }); 
    // }

}

module.exports = functions;
